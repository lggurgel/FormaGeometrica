#include "../inc/Quadrado.hpp"
#include "../inc/Circulo.hpp"
#include "../inc/Retangulo.hpp"

using namespace std;

int main (){

	void trabalhaCirculo();
	void trabalhaQuadrado();
	void trabalhaRetangulo();

	int menu;

	cout << "FORMAS GEOMETRICAS" << endl << endl;
	cout << "(1) Quadrado  (2) Retrangulo  (3) Circulo" << endl;
	cout << "Qual deseja trabalhar? :";
	cin >> menu;

	switch(menu){
		case 1:{
			trabalhaQuadrado();
			break;
		}
		case 2:{
			trabalhaRetangulo();
			break;
		}

		case 3:{
			trabalhaCirculo();
			break;
		}

		default:
			cout << "Entrada invalida" << endl; 
			break;
	}

	return 0;
}

void trabalhaQuadrado(){

	float lado;
	cout << "Comprimento do lado: ";
	cin >> lado;
	Quadrado quadrado(lado);

	cout << "Area do quadrado: " << quadrado.calculaArea() << endl;
}


void trabalhaRetangulo(){

	float comprimento, largura;
	cout << "Comprimento: ";
	cin >> comprimento;
	cout << "Largura: ";
	cin >> largura;
	Retangulo retrangulo(comprimento, largura);

	cout << "Area do quadrado: " << retrangulo.calculaArea() << endl;
}


void trabalhaCirculo(){

	float raio;
	cout << "Raio: ";
	cin >> raio;
	Circulo roda(raio);

	cout << "Comprimento: " << roda.getComprimento() << endl;
	cout << "Area: " << roda.calculaArea() << endl; 

}
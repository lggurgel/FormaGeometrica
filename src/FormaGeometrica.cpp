#include "../inc/FormaGeometrica.hpp"
#include <string>

using namespace std;

FormaGeometrica::FormaGeometrica(){

	this->comprimento = 0;
	this->largura = 0;

}

FormaGeometrica::FormaGeometrica(float comprimento, float largura){

	this->comprimento = comprimento;
	this->largura = largura;

}

FormaGeometrica::~FormaGeometrica(){}


float FormaGeometrica::calculaArea(){

	return comprimento * largura;

}


float FormaGeometrica::getComprimento(){
	return comprimento;
}

void FormaGeometrica::setComprimento(float comprimento){
	this->comprimento = comprimento;
}

float FormaGeometrica::getLargura(){

	return largura;
}

void FormaGeometrica::setLargura(float largura){
	this->largura = largura;
}
#include "../inc/Circulo.hpp"

using namespace std;

Circulo::Circulo(){

	this->raio = 0;

}

Circulo::Circulo(float raio){

	this->raio = raio;

}

Circulo::~Circulo(){}


float Circulo::getComprimento(){

	return 2 * pi * raio;
}

float Circulo::calculaArea(){

	return pi * raio * raio;

}

float Circulo::getRaio(){
	return this->raio;
}

void Circulo::setRaio(float raio){
	this->raio = raio;
}
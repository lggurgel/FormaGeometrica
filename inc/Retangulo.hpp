#ifndef RETANGULO_HPP
#define RETANGULO_HPP

#include "FormaGeometrica.hpp"

class Retangulo : public FormaGeometrica{

public:
	Retangulo();
	Retangulo(float comprimento, float largura);
	~Retangulo();


};
#endif
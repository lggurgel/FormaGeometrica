#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

#include <iostream>

class FormaGeometrica{

protected:
	float comprimento, largura;

public:
	FormaGeometrica();
	FormaGeometrica(float comprimento, float largura);
	~FormaGeometrica();


	float getComprimento();
	void setComprimento(float largura);

	float getLargura();
	void setLargura(float largura);

	float calculaArea();


};
#endif
#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "FormaGeometrica.hpp"

class Circulo : public FormaGeometrica{

private:
	
	float raio;
	float pi = 3.1415;

public:
	Circulo();
	Circulo(float raio);
	~Circulo();

	float getComprimento();
	float calculaArea();


	float getRaio();
	void setRaio(float raio);

};
#endif